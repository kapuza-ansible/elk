# ELK install
## Links
* [Elastic Stack and Product Documentation](https://www.elastic.co/guide/index.html)
* [ELK in wiki.salo.vip](https://wiki.salo.vip/index.php?title=Kon:services:elk)

## Install ELK in ubuntu
```bash
# Clone repo
git clone https://gitlab.com/kapuza-ansible/elk.git

# Cd to example
cd example

# Edit inventory
vim ./inventory/hosts

# Edit conf in yml
vim ./*.yml

# Install elastic
ansible-playbook ./elastic_install.yml

# Install kibana
ansible-playbook ./kibana_install.yml

# Install logstash
ansible-playbook ./logstash_install.yml
```

## Warn
This playbook only install soft.

## Todo
* Setup services
  * Elastic cluster
  * Logstash examples
  * Set security to elastic
  * Set ssl to elastic and kibana
* Check service status
* Backup settings
* Remove settings and services

